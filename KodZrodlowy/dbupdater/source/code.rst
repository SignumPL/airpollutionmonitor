Dokumentacja kodu zrodlowego
============================

Ponizsza dokumentacja obejmuje opis klas, metod i pol programu dbupdater.

Modul inicjujacy aplikacje
^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: main
    :members:

Glowne moduly programu
^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: BackgroundWorker
    :members:

.. automodule:: CustomHtmlParser
    :members:

.. automodule:: DatabaseUploader
    :members:

.. automodule:: DataDownloader
    :members:

Klasy pomocnicze
^^^^^^^^^^^^^^^^
.. automodule:: AirPollution
   :members:

.. automodule:: AirPollutionMeasurement
    :members:

.. automodule:: AirPollutionTypesContainer
    :members:

.. automodule:: HtmlParserContent
    :members: