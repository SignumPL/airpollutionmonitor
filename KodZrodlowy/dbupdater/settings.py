"""Klasa settings
    Zawiera ustawienia połączenia z bazą danych, ustawienia strefy czasowej i tym podobne.
"""


# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'vdh$wz=+mj^wq@aap4q*aq37myowtce)hnq!63b#*1hfrhirn#'

"Parametry polaczenia z baza danych - sterownik (data provider), nazwa hosta, login, haslo, nazwa bazy danych."
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'apm',
        'USER': 'postgres',
        'PASSWORD': '123',
        'HOST': '127.0.0.1'
    }
}

"Okresla strefe czasowa uzywana w aplikacji"
TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True