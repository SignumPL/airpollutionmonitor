"""Glowny modul odpowiedzialny za konfiguracje logowania, i uruchomienie glownej petli programu.

"""

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "settings")
import logging
from modules.BackgroundWorker import BackgroundWorker

def setup_loggers():
    """Metoda konfigurujaca logger - ustawia formatowanie i wyjscia dla logow.

    """
    log_formatter = logging.Formatter("%(asctime)s [%(levelname)s]  %(message)s")
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(log_formatter)
    logging.getLogger().setLevel(logging.DEBUG)
    logging.getLogger().addHandler(console_handler)
    logging.debug("Logger configured")

if __name__ == "__main__":
    setup_loggers()
    backgroundWorker = BackgroundWorker(60.0*15)
    backgroundWorker.run()

