class HtmlParserContent:
    """Instancja tej klasy przechowuje informacje o pomiarach, ktore zwraca modul DataDownloader.

    """

    Date = None
    "Data pomiaru"

    AirPollutionMeasurements = []
    "Lista obiektow przechowujacych dane o pomiarach"

    def __init__(self, date, air_pollution_measurements):
        """Metoda inicjujaca instancje klasy wartosciami podanymi jako parametry
            Args:

                    date: Data pomiaru

                    air_pollution_measurements: Lista obiektow przechowujacych dane o pomiarach

        """

        self.Date = date
        self.AirPollutionMeasurements = air_pollution_measurements