class AirPollutionMeasurement:
    """Klasa reprezentujaca wyniki pojedynczego pomiaru zanieczyszczenia powietrza

    """

    MeasuringStation = None
    "Nazwa stacji pomiarowej"

    AirPollutionType = None
    "Referencja do instancji klasy reprezentujacej rodzaj zanieczyszczenia powietrza"

    Hour = None
    "Godzina o ktorej dokonano pomiaru"

    Value = None
    "Wartosc zanieczyszczenia powietrza"

    def __init__(self, measuring_station, air_pollution_type, hour, value):
        """Metoda inicjujaca obiekt tej klasy wartoscami podanymi w parametrze.air_pollution_type

        Args:

         measuring_station: Nazwa stacji pomiarowej

         air_pollution_type: Rodzaj zanieczyszczenia powietrza

         hour: Godzina o ktorej dokonano pomiaru

         value: Wartosc zanieczyszczenia powietrza

        """
        self.MeasuringStation = measuring_station
        self.AirPollutionType = air_pollution_type
        self.Hour = hour
        self.Value = value