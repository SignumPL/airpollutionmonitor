import logging

class AirPollution:
    """Klasa defniniujaca zanieczyszczenie powietrza

    """

    Name = None
    "Nazwa zanieczyszczenia"

    StandardValue = None
    "Dopuszczalna wartosc"

    UrlCode = None
    "Kod URL z ktorego aplikacja pobiera dane"

    def __init__(self, description, url_code, standard_value):
        """Funkcja inicjujaca instance klasy podanymi w argumentach wartosciami

        Args:

            description: Nazwa zanieczyszczenia

            url_code: Kod URL

            standard_value: Dopuszczalna wartosc

        """
        self.Name = description
        self.UrlCode = url_code
        self.StandardValue = standard_value

    def match_description(self, description):
        """Metoda porownuje nazwe zanieczyszczenia tej instancji klasy, z parametrem podanym jako argument.

        """
        return description in self.Name

    def match_url_code(self, url_code):
        """Metoda porownuje kod URL z ktorego pobierane sa aktualne dane

        """
        return url_code in self.UrlCode