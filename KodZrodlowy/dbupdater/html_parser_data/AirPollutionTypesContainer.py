import logging
from html_parser_data.AirPollution import AirPollution

class AirPollutionTypesContainer:
    """Klasa przechowujaca domyslnie skonfigurowane definicje zanieczyszczen powietrza. Jest to klasa implementujaca
        wzorzec Singleton.

    """

    _singleton = None

    def __new__(cls, *args, **kwargs):
        if not cls._singleton:
            cls._singleton = super(AirPollutionTypesContainer,cls).__new__(cls, *args, **kwargs)
            cls.AirPollutionTypes = []
            cls.AirPollutionTypes.append(AirPollution("Dwutlenek siarki SO2", "01", 350))
            cls.AirPollutionTypes.append(AirPollution("Tlenek azotu NO", "02", -1))
            cls.AirPollutionTypes.append(AirPollution("Dwutlenek azotu NO2", "03", -1))
            #cls.AirPollutionTypes.append(AirPollution("Tlenek węgla CO", "04", -1))
            cls.AirPollutionTypes.append(AirPollution("Ozon O3", "08", -1))
            cls.AirPollutionTypes.append(AirPollution("Tlenki azotu NOx", "12", -1))
            cls.AirPollutionTypes.append(AirPollution("Pył zawieszony PM10", "24", -1))
            cls.AirPollutionTypes.append(AirPollution("Pył zawieszony PM2.5", "39", -1))
            #cls.AirPollutionTypes.append(AirPollution("Benzen C6H6", "V4", -1))
            logging.debug("Filled air pollution types container")

        return cls._singleton

    def get_by_url_code(self, url_code):
        """Zwraca definicje zanieczyszczenia powietrza jako instancje klasy, na podstawie kodu URL

        """
        result = next(x for x in self.AirPollutionTypes if x.match_url_code(url_code))
        return result

    def get_by_description(self, description):
        """Zwraca definicje zanieczyszczenia powietrza jako instancje klasy, na podstawie nazwy zanieczyszczenia powietrza

        """
        result = next(x for x in self.AirPollutionTypes if x.match_description(description))
        return result