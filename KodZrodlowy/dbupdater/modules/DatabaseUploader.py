import logging
from datetime import datetime
from django.utils.timezone import utc
from app.models import Station
from app.models import Pollution
from app.models import Measurement
from html_parser_data import HtmlParserContent

class DatabaseUploader:
    """Klasa odpowiadajaca za ladowanie wynikow pomiarow do bazy danych.

    """

    def update_database(self, html_parser_content):
        """Metoda laduje do bazy danych wyniki pomiarow zwrocone przez metode w klasie DataDownloader.

            Args:

                    html_parser_content: Lista obiektow zwroconych przez metode w klasie DataDownloader

        """

        logging.info("Updating database. This might take a while...")
        new_stations = []
        new_airpollutiontypes = []
        new_measurements_count = 0
        total_measurements_count = 0

        for content in html_parser_content:

            for measure in content.AirPollutionMeasurements:
                total_measurements_count+=1
                measure_timestamp = content.Date.replace(hour=measure.Hour, tzinfo=utc)

                db_station = Station.objects.filter(name=measure.MeasuringStation).first()
                if not db_station:
                    db_station = Station(name=measure.MeasuringStation)
                    db_station.save()
                    new_stations.append(measure.MeasuringStation)

                db_pollution = Pollution.objects.filter(name=measure.AirPollutionType.Name).first()
                if not db_pollution:
                    db_pollution = Pollution(name=measure.AirPollutionType.Name, warn_level = -1, danger_level = -1)
                    db_pollution.save()
                    new_airpollutiontypes.append(measure.AirPollutionType.Name)

                db_measurement = Measurement.objects.filter(timestamp=measure_timestamp, value=measure.Value, pollution=db_pollution, station=db_station).first()
                if not db_measurement:
                    db_measurement = Measurement(timestamp=measure_timestamp, value=measure.Value, pollution=db_pollution, station=db_station)
                    db_measurement.save()
                    new_measurements_count+=1

        logging.info("Finished updating database.\n\t\t\tNew measuring stations: {0}\n\t\t\tNew air pollution types: {1}\n\t\t\tNew measurements: {2} ({3} up to date)".format(", ".join(new_stations), ", ".join(new_airpollutiontypes), new_measurements_count, total_measurements_count-new_measurements_count))