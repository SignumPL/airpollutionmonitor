import logging
import time
from datetime import datetime, timedelta
from modules.DataDownloader import DataDownloader
from modules.CustomHtmlParser import CustomHtmlParser
from modules.DatabaseUploader import DatabaseUploader
from html_parser_data.AirPollutionTypesContainer import AirPollutionTypesContainer

class BackgroundWorker:
    """Obiekt tej klasy odpowiada za uruchamianie pozostalych komponentow aplikacji. Co okreslony czas uruchamia
    modul pobierajacy strone HTML, nastepnie parser HTML, a na koncu odwoluje sie do modulu ktory laduje dane
    do bazy danych.

    """

    def __init__(self, interval):
        """Metoda inincjujaca obiekt tej klasy. Tworzy instancje pozostalych modulow programu.

            Args:

                    interval: Parametr okresla co ile sekund program ma aktualizowac wyniki pomiarow.

        """
        logging.debug("Update interval set to {0}".format(interval))
        self.interval = interval
        self.data_downloader = DataDownloader()
        self.custom_html_parser = CustomHtmlParser()
        self.database_uploader = DatabaseUploader()
        self.air_pollution_types_container = AirPollutionTypesContainer()

    def run(self):
        """Metoda uruchamia nieskonczona petle, ktora co okreslony interwal aktualizuje wyniki pomiarow.


        """
        logging.info("Entering main loop! To exit at any moment press CTRL+C")
        while True:
            html_parser_content = []

            for air_pollution_type in self.air_pollution_types_container.AirPollutionTypes:
                html_content = self.data_downloader.download_html(air_pollution_type.UrlCode)
                html_parser_output = self.custom_html_parser.parse(air_pollution_type, html_content)
                html_parser_content.append(html_parser_output)

            self.database_uploader.update_database(html_parser_content)
            next_update = (datetime.now() + timedelta(seconds=self.interval)).strftime("%H:%M:%S")
            logging.debug("Next update in {0} seconds - at {1}".format(self.interval, next_update))
            time.sleep(self.interval)