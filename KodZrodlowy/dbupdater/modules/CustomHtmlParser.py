import logging
from bs4 import BeautifulSoup
from datetime import datetime
from html_parser_data.HtmlParserContent import HtmlParserContent
from html_parser_data.AirPollutionMeasurement import AirPollutionMeasurement
from html_parser_data.AirPollution import AirPollution

class CustomHtmlParser:
    """Klasa odpowiadajaca za parsowanie strony HTML.

    """

    def parse(self, air_pollution_type, html_content):
        """Metoda przetwarza podana w argumencie zawartosc strony HTML i zwraca wyniki pomiarow jako liste instancji klasy
        AirPollutionMeasurement.

            Args:

                    air_pollution_type: Rodzaj zanieczyszczenia powietrza ktorego dotycza pomiary

                    html_content: Zawartosc strony HTML jako ciag znakow

        """

        logging.info("Parsing HTML content for '{0}'".format(air_pollution_type.Name))
        soup = BeautifulSoup(html_content)

        # Parse measuring date
        measurement_date = datetime.strptime(soup.h3.string, " () -  %Y-%m-%d")

        table = soup.find_all("table")[0]
        table_trs = table.find_all("tr")
        table_trs_actual = table_trs[2:len(table_trs)]

        measurements = []

        for tr in table_trs_actual:
            tds = tr.find_all("td")
            measuring_station_name = tds[0].string.strip()

            hour = 0
            for measurement in tds[3:len(tds)-1]:
                trimmed_value = measurement.string.strip()
                hour+=1
                if not trimmed_value:
                    continue
                else:
                    measurements.append(AirPollutionMeasurement(measuring_station_name, air_pollution_type, hour, trimmed_value))

        logging.info("Successfuly parsed HTML contents. Stations: {0}   Measurements: {1}".format(len(table_trs_actual), len(measurements)))
        return HtmlParserContent(measurement_date, measurements)
