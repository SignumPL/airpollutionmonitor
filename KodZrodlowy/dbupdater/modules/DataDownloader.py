import logging
import urllib.request
from html_parser_data.AirPollutionTypesContainer import AirPollutionTypesContainer

class DataDownloader:
    """Klasa odpowiadajaca za pobieranie strony HTML.

    """

    def __init__(self):
        """Metoda ininicjujaca instancje klasy. Pobiera takze reference do singletona AirPollutionTypesContainer

        """
        self.air_pollution_types_container = AirPollutionTypesContainer()

    def download_html(self, url_code):
        """Pobiera zawartosc strony HTML ktorej link konczy sie okreslonym w parametrze kodem URL. Zwraca kod HTML w
        postaci ciagu znakow.

            Args:

                    url_code: Kod URL strony z wynikami pomiarow ktora nalezy pobrac

        """

        url = "http://monitoring.krakow.pios.gov.pl/iseo/aktualne_parametr.php?parametr=" + url_code
        air_pollution_description = self.air_pollution_types_container.get_by_url_code(url_code).Name

        logging.info("Downloading today's measurements for '{0}' from URL: {1}".format(air_pollution_description, url))

        reader = urllib.request.urlopen(url)
        content = reader.read()
        logging.info("Successfuly downloaded, data size: {0} bytes".format(len(content)))
        return content