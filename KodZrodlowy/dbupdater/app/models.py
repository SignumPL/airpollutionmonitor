from django.db import models

class Station(models.Model):
    name = models.TextField()

class Pollution(models.Model):
    name = models.TextField()
    description = models.TextField()
    warn_level = models.FloatField()
    danger_level = models.FloatField()

class Measurement(models.Model):
    timestamp = models.DateTimeField()
    value = models.FloatField()
    pollution = models.ForeignKey('Pollution')
    station = models.ForeignKey('Station')