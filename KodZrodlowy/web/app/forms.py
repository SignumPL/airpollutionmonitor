from django import forms

class ViewReportForm(forms.Form):
    """Klasa reprezentujaca formularz wyboru zakresu dat i stacji pomiarowej przez uzytkownika

    """


    stationid = forms.IntegerField(required=True)
    "Identyfikator stacji pomiarowej"

    start_date = forms.CharField(required=True)
    "Data poczatkowa na wykresie"

    end_date = forms.CharField(required=True)
    "Data koncowa na wykresie"