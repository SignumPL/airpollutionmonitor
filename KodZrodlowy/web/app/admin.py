from django.contrib import admin
from app.models import Pollution

class PostAdmin(admin.ModelAdmin):
    # fields display on change list
    list_display = ['name', 'description', 'warn_level', 'danger_level']
    # fields to filter the change list with
    list_filter = ['name', 'description', 'warn_level', 'danger_level']
    # fields to search in change list
    search_fields = ['name', 'description', 'warn_level', 'danger_level']
    # enable the date drill down on change list
    #date_hierarchy = 'created'
    # enable the save buttons on top on change form
    save_on_top = True
    # prepopulate the slug from the title - big timesaver!
    #prepopulated_fields = {"name": ("name",)}

admin.site.register(Pollution, PostAdmin)
