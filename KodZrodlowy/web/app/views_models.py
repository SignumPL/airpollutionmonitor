from app.models import Station, Pollution, Measurement

class IndexModel:
    """Model MVC widoku glownej strony. Zawiera liste dostepnych stacji pomiarowych.

    """

    def __init__(self, stations):
        """Metoda inicjujaca obiekt tej klasy.

            Args:

                    stations: Lista stacji pomiarowych.

        """

        self.Stations = stations

class PlotData:
    """Model MVC widoku wyswietlajacego wykres.

    """

    def __init__(self, file_path, station, start_date, end_date):
        """Metoda inicjujaca obiekt tej klasy.

            Args:

                    file_path: Sciezka URL do pliku z wygenerowanym wykresem

                    station: Nazwa stacji pomiarowej

                    start_date: Data poczatkowa na wykresie

                    end_date: data koncowa na wykresie

        """

        self.FilePath = file_path
        self.Station = station
        self.Station.__class__ = Station
        self.StartDate = start_date
        self.EndDate = end_date