from django.shortcuts import render
from django.http import HttpResponseRedirect
from app.models import Pollution, Station, Measurement
from app.views_models import PlotData
from app.plot_renderer import PlotRenderer
from app.forms import ViewReportForm
import datetime


plotRenderer = PlotRenderer()
"Instancja klasy PlotRenderer do tworzenia wykresow"


def index(request):
    """Obsluguje zadanie HTTP dla glownej strony index.html

    """

    form = ViewReportForm() # An unbound form
    now = datetime.datetime.utcnow()
    form.start_date = datetime.datetime(now.year, now.month, now.day, 1)
    form.end_date = datetime.datetime(now.year, now.month, now.day, now.hour)

    stations = Station.objects.all()
    return render(request, 'app/index.html', {'form': form, 'stations': stations})


def air_pollutions(request):
    return render(request, 'app/air_pollutions.html')

def stations(request):
    return render(request, 'app/stations.html')


def show_plot(request):
    """Obsluguje zadanie HTTP dla strony wyswietlajacej wykres. Obsluguje zarowno metody GET jak i POST.

    """

    if request.method == 'POST': # If the form has been submitted...
        # ContactForm was defined in the previous section
        form = ViewReportForm(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            station_id = form.cleaned_data['stationid']
            start_date = form.cleaned_data['start_date']
            end_date = form.cleaned_data['end_date']

            station = Station.objects.filter(id=station_id).first()
            from datetime import datetime
            date1 = datetime.strptime(start_date, "%Y-%m-%d %H:%M")
            date2 = datetime.strptime(end_date, "%Y-%m-%d %H:%M")

            plot_file_path = plotRenderer.render_plot(station, date1, date2)
            return render(request, 'app/show_plot.html', {'plot_file_path': plot_file_path})
        else:
            return HttpResponseRedirect('index.html', {'is_form_invalid': True})
    else:
        return HttpResponseRedirect('index.html')

