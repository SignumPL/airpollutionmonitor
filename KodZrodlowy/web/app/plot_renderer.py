import datetime
import airpollutionmonitor.settings
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import MultipleLocator, FormatStrFormatter, AutoMinorLocator, MaxNLocator
from matplotlib.dates import YearLocator, MonthLocator, DayLocator, HourLocator, MinuteLocator, SecondLocator, DateFormatter, AutoDateLocator
from app.models import Pollution, Station, Measurement

class PlotRenderer:
    """Klasa renderujaca wykres i zapisujaca go do pliku graficznego

    """

    def __init__(self):
        """Funkcja inicjujaca obiekt tej klasy. Najwazniejszy jej zadaniem jest utworzenie obiektu 'fig' dzieki ktoremu
        mozliwe jest rysowanie wykresu.

        """

        self.fig = plt.figure()

    def render_plot(self, station, start_date, end_date):
        """Funkcja renderujaca wykres.

            Args:

                    station: Referencja do klasy ktorej obiekt reprezentuje stacje pomiarowa

                    start_date: Data poczatkowa

                    end_date: Data koncowa

        """

        pollutions = Pollution.objects.all()
        #fig = plt.figure() # tu sie zawiesza!!!!
        ax = self.fig.add_subplot(111)
        self.fig.suptitle("{0} {1} - {2}".format(station.name, start_date, end_date))
        legend = []


        measurements_count = []

        for pollution in pollutions:
            measurements = Measurement.objects.filter(station=station,pollution=pollution,timestamp__range=(start_date, end_date))
            dates = []
            values = []
            for measurement in measurements:
                dates.append(measurement.timestamp)
                values.append(measurement.value)

            ax.plot_date(dates, values, '-', marker='o')
            legend.append(pollution.name)
            measurements_count.append(len(values))



        major_locator = AutoDateLocator(maxticks=12)
        minor_locator = AutoMinorLocator()
        date_formatter = DateFormatter('%Y-%m-%d %H:%M')

        ax.xaxis.set_major_locator(major_locator)
        ax.xaxis.set_minor_locator(minor_locator)
        ax.xaxis.set_major_formatter(date_formatter)
        ax.autoscale_view()
        plt.gca().yaxis.set_major_formatter(FormatStrFormatter("%d µg/m3"))

        # legend
        box = ax.get_position()
        ax.set_position([box.x0, box.y0 + box.height * 0.1, box.width, box.height * 0.9])
        font_legend = FontProperties()
        font_legend.set_size(7)
        ax.legend(legend, loc='upper center', bbox_to_anchor=(0.5, -0.13), fancybox=True, shadow=True, ncol=4, prop=font_legend)

        plt.tick_params(axis='both', which='major', labelsize=7)
        plt.tick_params(axis='both', which='minor', labelsize=4)

        self.fig.autofmt_xdate()
        filename = str(datetime.datetime.utcnow().strftime("%Y-%m-%d_%H.%M.%S.%f")) + ".png"
        plt.savefig("static/" + filename)

        plt.cla()
        plt.clf()
        #plt.close()

        return filename