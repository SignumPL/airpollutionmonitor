.. AirPollutionMonitorWeb documentation master file, created by
   sphinx-quickstart on Wed Jun 11 20:32:07 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Witamy w dokumentacji technicznej aplikacji Air Pollution Monitor Web!
======================================================================

Zawartość:

.. toctree::
   :maxdepth: 2

   about
   db_diagrams
   code

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

