Opis dzialania aplikacji
========================

Niniejsza aplikacja przeznaczona jest do generowania wykresów zanieczyszczenia powietrza na podstawie danych pomiarowych
z roznych stacji pomiarowych. Dane pobierane sa z oficjalnej strony http://monitoring.krakow.pios.gov.pl/iseo/aktualne_main.php

Obecnie dostepne sa nastepujace stacje pomiarowe:

- Kraków, Aleja Krasińskiego
- Kraków, Nowa Huta
- Kraków, Kurdwanów
- Zakopane
- Skawina
- Nowy Sącz
- Olkusz
- Trzebinia
- Szymbark
- Sucha Beskidzka
- Szarów

W zaleznosci od stacji pomiarowej, dostepne sa czujniki dla nastepujacych rodzajow zanieczyszczenia powietrza:

- Dwutlenek siarki
- Tlenek azotu
- Dwutlenek azotu
- Ozon
- Tlenki Azotu
- Pył zawieszony PM10
- Pył zawieszony PM2,5


Obsluga aplikacji sprowadza sie do wybrania stacji pomiarowej, oraz zakresu dat.
Przykladowy wykres:

.. figure::  sample_diagram.png
   :align:   center

Narzedzia i biblioteki wykorzystane w projekcie
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Aplikacja webowa bazuje na frameworku django, przeznaczonym do budowania interaktywnych stron internetowych z uzyciem
wzorca projektowego MVC.
Dane przechowywane sa w bazie danych PostgreSQL.
Do generowania wykresow wykorzystana zostala biblioteka matplotlib, oraz szereg mniejszych bibliotek od ktorych ona zalezy.
