Dokumentacja kodu zrodlowego
============================

Ponizsza dokumentacja obejmuje opis klas, metod i pol aplikacji Air Pollution Monitor Web.


Formularz opcji generowania wykresu
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: forms
    :members:


Generator wykresow
^^^^^^^^^^^^^^^^^^

.. automodule:: plot_renderer
    :members:


Obsluga widokow
^^^^^^^^^^^^^^^

.. automodule:: views
    :members:

Modele MVC
^^^^^^^^^^

.. automodule:: views_models
    :members:

