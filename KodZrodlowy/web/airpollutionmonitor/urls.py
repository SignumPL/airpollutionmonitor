from airpollutionmonitor import settings
from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'airpollutionmonitor.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^$', 'app.views.index'),
    url(r'^index.html', 'app.views.index'),
    url(r'^show_plot.html', 'app.views.show_plot'),
    url(r'^air_pollutions.html$', 'app.views.air_pollutions', name='air_pollutions'),
    url(r'^stations.html$', 'app.views.stations', name='stations'),
)
